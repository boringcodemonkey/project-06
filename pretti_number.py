import math
import multiprocessing as mp
import concurrent.futures
from tqdm import tqdm


numberOfCores = mp.cpu_count()


def check_if_perfect_square(a, b, c):
    n = int(a+b+c)
    n = abs(n)
    # print(n)

    if math.sqrt(n).is_integer() == True:
        return True
    else:
        return False


def check_if_perfect_cube(a, b, c, d):
    n = int(a+b+c+d)
    n = abs(n)
    return round(n ** (1 / 3)) ** 3 == n


def check_ifequal(a, b):
    if abs(int(a)) == abs(int(b)):
        return True
    else:
        return False


def isZero(a):
    if abs(int(a)) != 0:
        return False
    else:
        return True


pretti_number = set()
count = 0


def find_pretti_number(i):
    #     for i in range(1000000, 9999999+1):
    #     print(i)
    i = str(i)
    #     print(i[0])
    if check_if_perfect_square(i[0], i[1], i[2]) and check_if_perfect_cube(i[3], i[4], i[5], i[6]) and check_ifequal(i[3], i[-1]) and not isZero(i[-3]):
        i = int(i)
        # print(i)
#         pretti_number.add(i)
        return(i)


# pretti_number = set()


# def find_pretti_number():
#     count = 1
#     for i in range(start, end):
#         count += 1
#         print("processing {} / {}, remain {:.2f}%".format(count, total,
#               round((total-count)/total, 2)), end='\r', flush=True)
#         # print(i)
#         i = str(i)
#         #     print(i[0])

#         if check_if_perfect_square(i[0], i[1], i[2]) and check_if_perfect_cube(i[3], i[4], i[5], i[6]) and check_ifequal(i[3], i[-1]) and not isZero(i[-3]):
#             i = int(i)
#             pretti_number.add(i)
#             return(i)

# find_pretti_number()
# print("\n\n")
# print(pretti_number)


def main():
    start = 10000000
    end = 99999999+1
    totalProcess = end-start+1

    results = set()

    print(totalProcess)

    with tqdm(total=totalProcess) as progress:
        for i in range(start, end):
            results.add(find_pretti_number(i))
            progress.update()

    # with concurrent.futures.ProcessPoolExecutor(max_workers=1) as pool:
    #     #         results = list(tqdm(pool.map(find_pretti_number, [i for i in range(start, end)]), total=totalProcess))
    #     with tqdm(total=totalProcess) as progress:

    #         futures = []

    #         for i in range(start, end):
    #             future = pool.submit(find_pretti_number, i)
    #             future.add_done_callback(lambda p: progress.update())
    #             futures.append(future)

    #         results = []
    #         for future in futures:
    #             result = future.result()
    #             results.append(result)

    print(results)


if __name__ == "__main__":
    main()
